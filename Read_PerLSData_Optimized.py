import sqlite3
import sys
import pandas as pd
import json

con = sqlite3.connect("/eos/cms/store/group/comm_dqm//ML4DC_2019/UL2017_PerLsDQMIO_ZeroBias/ZeroBias_UL2017F_1-2D.sqlite")
#cursor = con.cursor()

run=True
count=15

while (run):

    query="SELECT metype,fromrun,fromlumi,value FROM monitorelements LIMIT 100000 OFFSET "+ str(count*100000)
    print(query)
    
    df=pd.read_sql_query(query, con)
        
    if(df.empty):
        print('Empty DF, Finish!') 
        run=False
        break
    
    df_final= df[['fromrun','fromlumi','metype']]

    df1 = pd.DataFrame.from_dict([x for x in df['value'].apply(json.loads)])
    dfx = pd.DataFrame.from_dict([x for x in df1['fXaxis']])
    dfy = pd.DataFrame.from_dict([x for x in df1['fYaxis']])

    df_final[['hname','histo', 'entries']]=df1[['fName','fArray','fEntries']].copy() 

    df_final[['Xmax','Xmin','Xbins']]=dfx[['fXmax','fXmin','fNbins']].copy()
    df_final[['Ymax','Ymin','Ybins']]=dfy[['fXmax','fXmin','fNbins']].copy()

    df_final.index=pd.MultiIndex.from_frame(df_final[['fromrun','fromlumi','hname']])
    count=count+1 

    fname="ZeroBias_2017F_DataFrame_1DAnd2D_" + str(count) + ".txt"    
    df_final.to_csv(fname)
    
    print('DONE!')
