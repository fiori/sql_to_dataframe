mport pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from ast import literal_eval
import glob

all_files = glob.glob("ZeroBias_2017F_DataFrame_*.txt")
#print(all_files)
#all_files = glob.glob("Azzo*.txt")
dfli=[]

for filename in all_files:
    df = pd.read_csv(filename, index_col=None)
    dfli.append(df)

df_plot = pd.concat(dfli, axis=0, ignore_index=True)    
    
#df_plot=pd.read_csv('ZeroBias_2017F_DataFrame_10.txt')
#print(df_plot.head())

df_new=df_plot.loc[(df_plot['fromrun']==305365) & (df_plot['hname'] == 'chargeInner_PXLayer_1'), ["hname", "histo", "fromlumi", "Xmin", "Xmax"]] 
df_new.index=df_new["fromlumi"]
df_new.sort_index(0,inplace=True) 

print(df_new.head(n=20))

def plotHist(num):
    
    global df_new
    
    ahisto=df_new['histo']
    #print(ahisto[df_new.index.get_level_values('fromlumi')[0]])
    name=df_new['hname']
    vmin=df_new['Xmin']
    vmax=df_new['Xmax']
    
    idxval=df_new.index.get_level_values('fromlumi')[num]
    
    histo=literal_eval(ahisto[idxval])
   
    x= vmin[idxval] + (np.arange(len(histo))) * float((vmax[idxval]-vmin[idxval])/float(len(histo)))
    plt.xlim(vmin[idxval],vmax[idxval])
    
    #srun=df_plot.index.get_level_values('fromrun')[num]
    #slumi=df_plot.index.get_level_values('fromlumi')[num]
    
    plt.step(x, histo, where='mid', label=(name[idxval] + " LS " + str(idxval) + " Run 305635"))
    #print(name[idxval] + " LS " + str(idxval) + " Run ")

for n in range(0,len(df_new['fromlumi'])):
    #print(n)
    plotHist(n)
    #print(float((vmax[num]-vmin[num])/len(histo)))
    
    #plt.title(name[num])

#plotHist(0)
#plotHist(3)
plt.legend()
plt.show()
